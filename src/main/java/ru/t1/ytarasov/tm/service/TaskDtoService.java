package ru.t1.ytarasov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.api.repository.ITaskDtoRepository;
import ru.t1.ytarasov.tm.api.service.ITaskDtoService;
import ru.t1.ytarasov.tm.dto.model.TaskDto;
import ru.t1.ytarasov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ytarasov.tm.exception.field.IdEmptyException;
import ru.t1.ytarasov.tm.exception.field.UserIdEmptyException;
import ru.t1.ytarasov.tm.exception.user.AccessDeniedException;

import java.util.List;

@Service
public class TaskDtoService implements ITaskDtoService {

    @NotNull
    @Autowired
    private ITaskDtoRepository taskDtoRepository;

    @Override
    @Transactional
    public void save(@NotNull final TaskDto task) {
        taskDtoRepository.save(task);
    }

    @Override
    @Transactional
    public void create() {
        @NotNull final TaskDto task = new TaskDto("New task " + System.currentTimeMillis(), "New task");
        taskDtoRepository.save(task);
    }

    @Override
    public List<TaskDto> findAll() {
        return taskDtoRepository.findAll();
    }

    @Override
    public TaskDto findById(@NotNull final String id) {
        return taskDtoRepository.findById(id).orElse(null);
    }

    @Override
    public long count() {
        return taskDtoRepository.count();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return taskDtoRepository.existsById(id);
    }

    @Override
    @Transactional
    public void delete(@NotNull final TaskDto task) {
        taskDtoRepository.delete(task);
    }

    @Override
    @Transactional
    public void deleteById(@NotNull final String id) {
        taskDtoRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void deleteAll(@NotNull final List<TaskDto> tasks){
        taskDtoRepository.deleteAll(tasks);
    }

    @Override
    @Transactional
    public void clear() {
        taskDtoRepository.deleteAll();
    }

    @Override
    @Transactional
    public TaskDto createWithUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final TaskDto task = new TaskDto("New task " + System.currentTimeMillis(), "New task");
        task.setUserId(userId);
        taskDtoRepository.saveAndFlush(task);
        return task;
    }

    @Override
    @Transactional
    public void saveWithUserId(@Nullable final String userId, @Nullable final TaskDto task) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new TaskNotFoundException();
        task.setUserId(userId);
        taskDtoRepository.save(task);
    }

    @Override
    public List<TaskDto> findByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return taskDtoRepository.findByUserId(userId);
    }

    @Override
    public TaskDto findByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskDtoRepository.findByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void deleteByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        taskDtoRepository.deleteByUserId(userId);
    }

    @Override
    @Transactional
    public void deleteByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        taskDtoRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public Long countByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return taskDtoRepository.countByUserId(userId);
    }

    @Override
    public boolean existsByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskDtoRepository.existsByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void deleteWithUserId(@Nullable final String userId, @Nullable final TaskDto task) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new TaskNotFoundException();
        if (!userId.equals(task.getUserId())) throw new AccessDeniedException();
        taskDtoRepository.delete(task);
    }

    @Override
    @Transactional
    public void deleteAllWithUserId(@Nullable final String userId, @Nullable final List<TaskDto> tasks) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (tasks == null || tasks.isEmpty()) throw new TaskNotFoundException();
        for (@NotNull final TaskDto task : tasks) {
            if (!userId.equals(task.getUserId())) throw new AccessDeniedException();
        }
        taskDtoRepository.deleteAll(tasks);
    }

}
