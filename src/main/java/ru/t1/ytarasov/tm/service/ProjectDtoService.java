package ru.t1.ytarasov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.api.repository.IProjectDtoRepository;
import ru.t1.ytarasov.tm.api.service.IProjectDtoService;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;
import ru.t1.ytarasov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ytarasov.tm.exception.field.IdEmptyException;
import ru.t1.ytarasov.tm.exception.field.UserIdEmptyException;
import ru.t1.ytarasov.tm.exception.user.AccessDeniedException;
import ru.t1.ytarasov.tm.exception.user.UserNotFoundException;

import java.util.List;

@Service
public class ProjectDtoService implements IProjectDtoService {

    @NotNull
    @Autowired
    private IProjectDtoRepository projectDtoRepository;

    @Override
    @Transactional
    public void save(@NotNull final ProjectDto project) {
        projectDtoRepository.save(project);
    }

    @Override
    @Transactional
    public void create() {
        @NotNull final ProjectDto project = new ProjectDto("New project " + System.currentTimeMillis(),
                "New project");
        projectDtoRepository.saveAndFlush(project);
    }

    @Override
    public List<ProjectDto> findAll() {
        return projectDtoRepository.findAll();
    }

    @Override
    public ProjectDto findById(@NotNull final String id) {
        return projectDtoRepository.findById(id).orElse(null);
    }

    @Override
    public long count() {
        return projectDtoRepository.count();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return projectDtoRepository.existsById(id);
    }

    @Override
    @Transactional
    public void delete(@NotNull final ProjectDto project) {
        projectDtoRepository.delete(project);
    }

    @Override
    @Transactional
    public void deleteById(@NotNull final String id) {
        projectDtoRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void deleteAll(@NotNull final List<ProjectDto> projects) {
        projectDtoRepository.deleteAll(projects);
    }

    @Override
    @Transactional
    public void clear() {
        projectDtoRepository.deleteAll();
    }

    @Override
    @Transactional
    public ProjectDto createWithUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ProjectDto project = new ProjectDto("New project " + System.currentTimeMillis(),
                "New project");
        project.setUserId(userId);
        projectDtoRepository.saveAndFlush(project);
        return project;
    }

    @Override
    @Transactional
    public void saveWithUserId(@Nullable final String userId, @Nullable final ProjectDto project) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ProjectNotFoundException();
        project.setUserId(userId);
        projectDtoRepository.save(project);
    }

    @Override
    public List<ProjectDto> findByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectDtoRepository.findByUserId(userId);
    }

    @Override
    public ProjectDto findByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectDtoRepository.findByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void deleteByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        projectDtoRepository.deleteByUserId(userId);
    }

    @Override
    @Transactional
    public void deleteByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        projectDtoRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public Long countByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectDtoRepository.countByUserId(userId);
    }

    @Override
    public boolean existsByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectDtoRepository.existsByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void deleteWithUserId(@Nullable final String userId, @Nullable final ProjectDto project) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (project == null) throw new ProjectNotFoundException();
        if (!userId.equals(project.getUserId())) throw new AccessDeniedException();
        projectDtoRepository.delete(project);
    }

    @Override
    @Transactional
    public void deleteAllWithUserId(@Nullable final String userId, @Nullable final List<ProjectDto> projects) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (projects == null || projects.isEmpty()) throw new ProjectNotFoundException();
        for (@NotNull final ProjectDto project : projects) {
            if (!userId.equals(project.getUserId())) throw new AccessDeniedException();
        }
        projectDtoRepository.deleteAll(projects);
    }

}
