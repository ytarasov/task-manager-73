package ru.t1.ytarasov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.dto.model.TaskDto;

import java.util.List;

@Repository
public interface ITaskDtoRepository extends JpaRepository<TaskDto, String> {

    List<TaskDto> findByUserId(@NotNull final String userId);

    TaskDto findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    Long countByUserId(@NotNull final String userId);

    Boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}
