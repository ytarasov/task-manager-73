package ru.t1.ytarasov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;

import java.util.List;

public interface IProjectDtoService {

    @Transactional
    void save(@NotNull ProjectDto project);

    @Transactional
    void create();

    List<ProjectDto> findAll();

    ProjectDto findById(@NotNull String id);

    long count();

    boolean existsById(@NotNull String id);

    @Transactional
    void delete(@NotNull ProjectDto project);

    @Transactional
    void deleteById(@NotNull String id);

    @Transactional
    void deleteAll(@NotNull List<ProjectDto> projects);

    @Transactional
    void clear();

    @Transactional
    ProjectDto createWithUserId(@Nullable String userId) throws Exception;

    @Transactional
    void saveWithUserId(@Nullable String userId, @Nullable ProjectDto project) throws Exception;

    List<ProjectDto> findByUserId(@Nullable String userId) throws Exception;

    ProjectDto findByUserIdAndId(@Nullable String userId, @Nullable String id) throws Exception;

    @Transactional
    void deleteByUserId(@Nullable String userId) throws Exception;

    @Transactional
    void deleteByUserIdAndId(@Nullable String userId, @Nullable String id) throws Exception;

    Long countByUserId(@Nullable String userId) throws Exception;

    boolean existsByUserIdAndId(@Nullable String userId, @Nullable String id) throws Exception;

    @Transactional
    void deleteWithUserId(@Nullable String userId, @Nullable ProjectDto project) throws Exception;

    @Transactional
    void deleteAllWithUserId(@Nullable String userId, @Nullable List<ProjectDto> projects) throws Exception;
}
