package ru.t1.ytarasov.tm.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import ru.t1.ytarasov.tm.dto.model.Message;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class AuthenticationEntryPointConfiguration extends BasicAuthenticationEntryPoint {

    @Override
    public void afterPropertiesSet() {
        setRealmName("ytarasov");
        super.afterPropertiesSet();
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        @NotNull final PrintWriter writer = response.getWriter();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String value = authException.getMessage();
        @NotNull final Message message = new Message(value);
        writer.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(message));
    }

}
