<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h1>PROJECTS</h1>
<table>
	<th>ID</th>
	<th>Name</th>
	<th>Description</th>
	<th>Created</th>
	<th>Updated</th>
	<th>Date begin</th>
	<th>Date end</th>
	<th>Status</th>
	<th>Edit</th>
	<th>Delete</th>
	<c:forEach var="project" items="${projects}">
	<tr>
		<td><c:out value="${project.id}" /></td>
		<td><c:out value="${project.name}" /></td>
		<td><c:out value="${project.description}" /></td>
		<td><fmt:formatDate pattern="dd.MM.yyyy" value="${project.created}" /></td>
		<td><fmt:formatDate pattern="dd.MM.yyyy" value="${project.updated}" /></td>
		<td><fmt:formatDate pattern="dd.MM.yyyy" value="${project.dateStart}" /></td>
		<td><fmt:formatDate pattern="dd.MM.yyyy" value="${project.dateFinish}" /></td>
		<td><c:out value="${project.status.displayName}" /></td>
		<td><a href="/project/edit/${project.id}">EDIT</a></td>
		<td><a href="/project/delete/${project.id}">DELETE</a></td>
	</tr>
	</c:forEach>
</table>
<form action="/project/create" method="POST" style="margin-top: 20px">
    <button>CREATE</button>
</form>

<jsp:include page="../include/_footer.jsp"/>