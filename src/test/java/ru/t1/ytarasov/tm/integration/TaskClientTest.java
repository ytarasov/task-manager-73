package ru.t1.ytarasov.tm.integration;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.t1.ytarasov.tm.dto.model.TaskDto;
import ru.t1.ytarasov.tm.dto.model.Result;
import ru.t1.ytarasov.tm.marker.WebIntegrationCategory;

import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(WebIntegrationCategory.class)
public class TaskClientTest {

    private static final String BASE_URL = "http://localhost:8080/api/tasks";

    @NotNull
    private static HttpHeaders HEADERS = new HttpHeaders();

    @NotNull
    private static String SESSION_ID;

    @NotNull
    private TaskDto task1;

    private static ResponseEntity<TaskDto> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity entity
    ) {
        @NotNull final RestTemplate template = new RestTemplate();
        return template.exchange(url, method, entity, TaskDto.class);
    }

    @BeforeClass
    public static void setUpBefore() throws Exception {
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final String authUrl = "http://localhost:8080/api/auth/login?login=test&password=test";
        @NotNull final ResponseEntity<Result> response = template.getForEntity(authUrl, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders responseHeaders = response.getHeaders();
        final List<HttpCookie> cookies =
                HttpCookie.parse(responseHeaders.getFirst(HttpHeaders.SET_COOKIE));
        SESSION_ID = cookies.stream()
                .filter(httpCookie -> "JSESSIONID".equals(httpCookie.getName()))
                .findFirst()
                .get()
                .getValue();
        Assert.assertNotNull(SESSION_ID);
        HEADERS.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + SESSION_ID));
        HEADERS.setContentType(MediaType.APPLICATION_JSON);
    }

    @AfterClass
    public static void setUpAfter() {
        @NotNull final String logoutUrl = "http://localhost:8080/api/auth/logout";
        sendRequest(logoutUrl, HttpMethod.POST, new HttpEntity<>(HEADERS));
    }

    @Before
    public void setUp() {
        @NotNull final String createUrl = BASE_URL + "/create";
        @NotNull final ResponseEntity<TaskDto> response = sendRequest(
                createUrl,
                HttpMethod.PUT,
                new HttpEntity<>(HEADERS)
        );
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        task1 = response.getBody();
        Assert.assertNotNull(task1);
    }

    @After
    public void tearDown() throws Exception {
        @NotNull final String deleteAllUrl = BASE_URL + "/clear";
        sendRequest(deleteAllUrl, HttpMethod.DELETE, new HttpEntity<>(HEADERS));
    }

    @Test
    public void create() {
        @NotNull final String url = BASE_URL + "/create";
        @NotNull final ResponseEntity<TaskDto> response = sendRequest(
                url,
                HttpMethod.PUT,
                new HttpEntity<>(HEADERS)
        );
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @NotNull final TaskDto TaskDto = response.getBody();
        Assert.assertNotNull(TaskDto);
    }

    @Test
    public void findById() {
        @NotNull final String taskId = task1.getId();
        @NotNull final String findByIdUrl = BASE_URL + "/findById/" + taskId;
        @NotNull final ResponseEntity<TaskDto> response = sendRequest(findByIdUrl, HttpMethod.GET, new HttpEntity<>(HEADERS));
        Assert.assertNotNull(response.getBody());
        @NotNull final TaskDto TaskDto = response.getBody();
        Assert.assertEquals(taskId, TaskDto.getId());
    }

    @Test
    public void findAll() {
        @NotNull final String findAllUrl = BASE_URL + "/findAll";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final ResponseEntity<TaskDto[]> response =
                (template.exchange(findAllUrl, HttpMethod.GET, new HttpEntity<>(HEADERS), TaskDto[].class));
        Assert.assertNotNull(response.getBody());
        TaskDto[] tasks = response.getBody();
        Assert.assertTrue(Arrays.stream(tasks).anyMatch(p -> task1.getId().equals(p.getId())));
    }

    @Test
    public void save() {
        @NotNull final String updateUrl = BASE_URL + "/save";
        @NotNull final String taskId = task1.getId();
        task1.setName("TEST NAME");
        sendRequest(updateUrl, HttpMethod.POST, new HttpEntity<TaskDto>(task1, HEADERS));
        @NotNull final String findUrl = BASE_URL + "/findById/" + taskId;
        @NotNull final ResponseEntity<TaskDto> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS));
        Assert.assertNotNull(response.getBody());
        @NotNull final TaskDto TaskDto = response.getBody();
        Assert.assertEquals("TEST NAME", TaskDto.getName());
    }

    @Test
    public void deleteById() {
        @NotNull final String taskId = task1.getId();
        @NotNull final String deleteByIdUrl = BASE_URL + "/deleteById/" + taskId;
        @NotNull final String findByIdUrl = BASE_URL + "/findById/" + taskId;
        @NotNull ResponseEntity<TaskDto> response = sendRequest(findByIdUrl, HttpMethod.GET, new HttpEntity<>(HEADERS));
        Assert.assertNotNull(response.getBody());
        @NotNull final TaskDto TaskDto = response.getBody();
        Assert.assertEquals(taskId, TaskDto.getId());
        sendRequest(deleteByIdUrl, HttpMethod.DELETE, new HttpEntity<>(HEADERS));
        response = sendRequest(findByIdUrl, HttpMethod.GET, new HttpEntity<>(HEADERS));
        Assert.assertNull(response.getBody());
    }

    @Test
    public void clear() {
        @NotNull final List<TaskDto> tasks = new ArrayList<>();
        tasks.add(task1);
        @NotNull final String taskId = task1.getId();
        @NotNull final String deleteAllUrl = BASE_URL + "/clear";
        @NotNull final String findByIdUrl = BASE_URL + "/findById/" + taskId;
        @NotNull ResponseEntity<TaskDto> response = sendRequest(findByIdUrl, HttpMethod.GET, new HttpEntity<>(HEADERS));
        Assert.assertNotNull(response.getBody());
        @NotNull final TaskDto TaskDto = response.getBody();
        Assert.assertEquals(taskId, TaskDto.getId());
        sendRequest(deleteAllUrl, HttpMethod.DELETE, new HttpEntity<>(tasks, HEADERS));
        response = sendRequest(findByIdUrl, HttpMethod.GET, new HttpEntity<>(HEADERS));
        Assert.assertNull(response.getBody());
    }

}
