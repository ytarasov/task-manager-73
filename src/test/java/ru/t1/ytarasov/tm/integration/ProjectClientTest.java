package ru.t1.ytarasov.tm.integration;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;
import ru.t1.ytarasov.tm.dto.model.Result;
import ru.t1.ytarasov.tm.marker.WebIntegrationCategory;

import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(WebIntegrationCategory.class)
public class ProjectClientTest {

    private static final String BASE_URL = "http://localhost:8080/api/projects";

    @NotNull
    private static HttpHeaders HEADERS = new HttpHeaders();

    @NotNull
    private static String SESSION_ID;

    @NotNull
    private ProjectDto project1;

    @BeforeClass
    public static void setUpBefore() throws Exception {
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final String authUrl = "http://localhost:8080/api/auth/login?login=test&password=test";
        @NotNull final ResponseEntity<Result> response = template.getForEntity(authUrl, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders responseHeaders = response.getHeaders();
        final List<HttpCookie> cookies =
                HttpCookie.parse(responseHeaders.getFirst(HttpHeaders.SET_COOKIE));
        SESSION_ID = cookies.stream()
                .filter(httpCookie -> "JSESSIONID".equals(httpCookie.getName()))
                .findFirst()
                .get()
                .getValue();
        Assert.assertNotNull(SESSION_ID);
        HEADERS.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + SESSION_ID));
        HEADERS.setContentType(MediaType.APPLICATION_JSON);
    }

    @AfterClass
    public static void tearDownAfter() {
        @NotNull final String logoutUrl = "http://localhost:8080/api/auth/logout";
        sendRequest(logoutUrl, HttpMethod.POST, new HttpEntity<>(HEADERS));
    }

    private static ResponseEntity<ProjectDto> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity entity
    ) {
        @NotNull final RestTemplate template = new RestTemplate();
        return template.exchange(url, method, entity, ProjectDto.class);
    }

    @Before
    public void setUp() {
        @NotNull final String createUrl = BASE_URL + "/create";
        @NotNull final ResponseEntity<ProjectDto> response = sendRequest(
                createUrl,
                HttpMethod.PUT,
                new HttpEntity<>(HEADERS)
        );
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        project1 = response.getBody();
        Assert.assertNotNull(project1);
    }

    @After
    public void tearDown() throws Exception {
        @NotNull final String deleteAllUrl = BASE_URL + "/clear";
        sendRequest(deleteAllUrl, HttpMethod.DELETE, new HttpEntity<>(HEADERS));
    }

    @Test
    public void create() {
        @NotNull final String url = BASE_URL + "/create";
        @NotNull final ResponseEntity<ProjectDto> response = sendRequest(
                url,
                HttpMethod.PUT,
                new HttpEntity<>(HEADERS)
        );
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @NotNull final ProjectDto projectDto = response.getBody();
        Assert.assertNotNull(projectDto);
    }

    @Test
    public void findById() {
        @NotNull final String projectId = project1.getId();
        @NotNull final String findByIdUrl = BASE_URL + "/findById/" + projectId;
        @NotNull final ResponseEntity<ProjectDto> response = sendRequest(findByIdUrl, HttpMethod.GET, new HttpEntity<>(HEADERS));
        Assert.assertNotNull(response.getBody());
        @NotNull final ProjectDto projectDto = response.getBody();
        Assert.assertEquals(projectId, projectDto.getId());
    }

    @Test
    public void findAll() {
        @NotNull final String findAllUrl = BASE_URL + "/findAll";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final ResponseEntity<ProjectDto[]> response =
                (template.exchange(findAllUrl, HttpMethod.GET, new HttpEntity<>(HEADERS), ProjectDto[].class));
        Assert.assertNotNull(response.getBody());
        ProjectDto[] projects = response.getBody();
        Assert.assertTrue(Arrays.stream(projects).anyMatch(p -> project1.getId().equals(p.getId())));
    }

    @Test
    public void save() {
        @NotNull final String saveUrl = BASE_URL + "/save";
        @NotNull final String projectId = project1.getId();
        project1.setName("TEST NAME");
        sendRequest(saveUrl, HttpMethod.POST, new HttpEntity<ProjectDto>(project1, HEADERS));
        @NotNull final String findUrl = BASE_URL + "/findById/" + projectId;
        @NotNull final ResponseEntity<ProjectDto> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS));
        Assert.assertNotNull(response.getBody());
        @NotNull final ProjectDto projectDto = response.getBody();
        Assert.assertEquals("TEST NAME", projectDto.getName());
    }

    @Test
    public void deleteById() {
        @NotNull final String projectId = project1.getId();
        @NotNull final String deleteByIdUrl = BASE_URL + "/deleteById/" + projectId;
        @NotNull final String findByIdUrl = BASE_URL + "/findById/" + projectId;
        @NotNull ResponseEntity<ProjectDto> response = sendRequest(findByIdUrl, HttpMethod.GET, new HttpEntity<>(HEADERS));
        Assert.assertNotNull(response.getBody());
        @NotNull final ProjectDto projectDto = response.getBody();
        Assert.assertEquals(projectId, projectDto.getId());
        sendRequest(deleteByIdUrl, HttpMethod.DELETE, new HttpEntity<>(HEADERS));
        response = sendRequest(findByIdUrl, HttpMethod.GET, new HttpEntity<>(HEADERS));
        Assert.assertNull(response.getBody());
    }

    @Test
    public void clear() {
        @NotNull final List<ProjectDto> projects = new ArrayList<>();
        projects.add(project1);
        @NotNull final String projectId = project1.getId();
        @NotNull final String deleteAllUrl = BASE_URL + "/clear";
        @NotNull final String findByIdUrl = BASE_URL + "/findById/" + projectId;
        @NotNull ResponseEntity<ProjectDto> response = sendRequest(findByIdUrl, HttpMethod.GET, new HttpEntity<>(HEADERS));
        Assert.assertNotNull(response.getBody());
        @NotNull final ProjectDto projectDto = response.getBody();
        Assert.assertEquals(projectId, projectDto.getId());
        sendRequest(deleteAllUrl, HttpMethod.DELETE, new HttpEntity<>(projects, HEADERS));
        response = sendRequest(findByIdUrl, HttpMethod.GET, new HttpEntity<>(HEADERS));
        Assert.assertNull(response.getBody());
    }

}
