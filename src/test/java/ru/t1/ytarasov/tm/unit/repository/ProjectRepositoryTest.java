package ru.t1.ytarasov.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.api.repository.IProjectDtoRepository;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;
import ru.t1.ytarasov.tm.marker.WebUnitCategory;

import java.util.List;
import java.util.UUID;

@Transactional
@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(WebUnitCategory.class)
public class ProjectRepositoryTest {

    @NotNull
    private static final String PROJECT_NAME_1 = "TEST PROJECT 1";

    @NotNull
    private static final String PROJECT_NAME_2 = "TEST PROJECT 2";

    @NotNull
    private ProjectDto project1;

    @NotNull
    private ProjectDto project2;

    @NotNull
    private static final String USER_ID = UUID.randomUUID().toString();

    @NotNull
    @Autowired
    private IProjectDtoRepository projectDtoRepository;

    @Before
    public void setUp() {
        project1 = new ProjectDto(PROJECT_NAME_1, PROJECT_NAME_1);
        project2 = new ProjectDto(PROJECT_NAME_2, PROJECT_NAME_2);
        project1.setUserId(USER_ID);
        project2.setUserId(USER_ID);
        projectDtoRepository.save(project1);
        projectDtoRepository.save(project2);
    }

    @After
    public void tearDown() {
        projectDtoRepository.deleteByUserId(USER_ID);
    }

    @Test
    public void findByUserIdAndId() {
        @Nullable final ProjectDto foundProject = projectDtoRepository.findByUserIdAndId(USER_ID, project1.getId());
        Assert.assertNotNull(foundProject);
        Assert.assertEquals(project1.getName(), foundProject.getName());
    }

    @Test
    public void findByUserId() {
        @Nullable final List<ProjectDto> projects = projectDtoRepository.findByUserId(USER_ID);
        Assert.assertNotNull(projects);
        Assert.assertFalse(projects.isEmpty());
    }

    @Test
    public void countByUserId() {
        @Nullable final List<ProjectDto> projects = projectDtoRepository.findByUserId(USER_ID);
        Assert.assertNotNull(projects);
        final int expectedSize = projects.size();
        final int foundSize = projectDtoRepository.countByUserId(USER_ID).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void existsByIdAndUserId() {
        Assert.assertTrue(projectDtoRepository.existsByUserIdAndId(USER_ID, project1.getId()));
    }

    @Test
    public void deleteByUserIdAndId() {
        @NotNull final ProjectDto projectToDelete = new ProjectDto("TEST DELETE", "TEST DELETE");
        projectToDelete.setUserId(USER_ID);
        projectDtoRepository.save(projectToDelete);
        final int expectedSize = projectDtoRepository.countByUserId(USER_ID).intValue() - 1;
        projectDtoRepository.deleteByUserIdAndId(USER_ID, projectToDelete.getId());
        final int foundSize = projectDtoRepository.countByUserId(USER_ID).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void deleteByUserId() {
        @NotNull final ProjectDto projectToDelete1 = new ProjectDto("TEST DELETE 1", "TEST DELETE");
        @NotNull final ProjectDto projectToDelete2 = new ProjectDto("TEST DELETE 2", "TEST DELETE");
        @NotNull final String userIdToDelete = UUID.randomUUID().toString();
        Assert.assertNotEquals(userIdToDelete, USER_ID);
        projectToDelete1.setUserId(userIdToDelete);
        projectToDelete2.setUserId(userIdToDelete);
        projectDtoRepository.save(projectToDelete1);
        projectDtoRepository.save(projectToDelete2);
        final int expectedSize = projectDtoRepository.countByUserId(USER_ID).intValue() - 2;
        projectDtoRepository.deleteByUserId(USER_ID);
        final int foundSize = projectDtoRepository.countByUserId(USER_ID).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

}
