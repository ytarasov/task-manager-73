package ru.t1.ytarasov.tm.unit.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.ytarasov.tm.api.service.ITaskDtoService;
import ru.t1.ytarasov.tm.dto.model.TaskDto;
import ru.t1.ytarasov.tm.marker.WebUnitCategory;
import ru.t1.ytarasov.tm.util.UserUtil;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(WebUnitCategory.class)
public class TaskControllerTest {

    @NotNull
    private static final TaskDto task1 = new TaskDto("TEST TASK 1", "TEST TASK 1");

    @NotNull
    private static final TaskDto task2 = new TaskDto("TEST TASK 2", "TEST TASK 2");

    @NotNull
    @Autowired
    private ITaskDtoService taskDtoService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private MockMvc mockMvc;

    @Nullable
    private String userId;

    private int count(@NotNull final String userId) throws Exception {
        return taskDtoService.countByUserId(userId).intValue();
    }

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        taskDtoService.saveWithUserId(userId, task1);
        taskDtoService.saveWithUserId(userId, task2);
    }

    @After
    public void tearDown() throws Exception {
        taskDtoService.deleteByUserId(userId);
    }

    @Test
    public void findAllTest() throws Exception {
        @NotNull final String url = "/tasks";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void createTest() throws Exception {
        @NotNull final String url = "/task/create";
        final int expectedSize = count(userId) + 1;
        mockMvc.perform(MockMvcRequestBuilders.post(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        final int foundSize = count(userId);
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void deleteTest() throws Exception {
        @NotNull final String url = "/task/delete/" + task2.getId();
        final int expectedSize = count(userId) - 1;
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        final int foundSize = count(userId);
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void editTest() throws Exception {
        @NotNull final String url = "/task/edit/" + task1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

}
